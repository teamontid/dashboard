<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ExcelFile extends Model
{
    protected $table = 'user_uploads_data';
    public $timestamps = false;
}
