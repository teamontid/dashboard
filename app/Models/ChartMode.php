<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ChartMode extends Model
{
    protected $table = 'chart_mode';
    public $timestamps = false;
}
